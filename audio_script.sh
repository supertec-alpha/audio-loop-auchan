#!/bin/bash

# Main audio loop script
# Lists all audio devices connected on the raspberry and play the audio track on each one
# This script only runs once and does not stay once ran so there is no need to check for previous instance of this script when it is restarted

# Kill all running instances of OMXPlayer on script start/restart to avoid audio channel conflicts
sudo killall omxplayer.bin

# Lists all possible audio devices and saves it in text file
(aplay -L | grep "^hw:.*$") > /home/pi/Documents/audio-loop-auchan/audiolist
# Counts number of detected audio devices
OUTS=$(wc -l < /home/pi/Documents/audio-loop-auchan/audiolist)

for i in `seq 0 $OUTS`
do
	echo $i
	# Start OMXPlayer instance to play on specified audio device in a loop
	omxplayer --no-keys -o alsa:hw:$i /home/pi/Documents/audio-loop-auchan/assets/audio.mp3 --loop &
done
