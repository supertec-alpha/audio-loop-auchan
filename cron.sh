#!/bin/bash

# Script ran to update the number of detected audio devices

# Get current count of registered audio devices
CUR=$(sudo wc -l < /home/pi/Documents/audio-loop-auchan/audiolist)
# Refresh audio devices list
sudo (aplay -L | grep "^hw:.*$") > /home/pi/Documents/audio-loop-auchan/audiolist
# Get new count of detected audio devices
OUTS=$(wc -l < /home/pi/Documents/audio-loop-auchan/audiolist)

# If old and new audio device count do not match restart launch audio script to refresh OMXPlayer instances
if [ $CUR -ne $OUTS ]
then
	# Run audio script
	sudo /home/pi/Documents/audio-loop-auchan/audio_script.sh&
	# May be superfluous
	sudo killall omxplayer.bin
fi
