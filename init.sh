#!/bin/bash

# Run at boot
# Resets previous run informations, checks for script updates and then starts the audio loop

# Remove audio devices list from previous boot
sudo cp /dev/null /home/pi/Documents/audio-loop-auchan/audiolist
# Check for script update
sudo git -C /home/pi/Documents/audio-loop-auchan/ pull origin master
# Run audio script
sudo /home/pi/Documents/audio-loop-auchan/audio_script.sh &
