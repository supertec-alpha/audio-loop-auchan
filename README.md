# Audio loop scripts

## Description

Automatically starts audio loop on all outputs at raspberry boot.
Checks hourly for new audio outputs.

## Setup

To install on **Raspberry Pi 3 Model B** flash micro SD card with clean raspbian stretch: 
- wget https://downloads.raspberrypi.org/raspbian/images/raspbian-2017-08-17/2017-08-16-raspbian-stretch.zip (stretch)

To install on **Raspberry Pi 3 Model B Plus** flash micro SD card with clean raspbian buster:
- wget https://downloads.raspberrypi.org/raspbian/images/raspbian-2020-02-14/2020-02-13-raspbian-buster.zip (buster)

Once the card is flashed, put it into your raspberry pi, plug a keyboard and boot.
Make sure your raspberry is connected to the internet.

Once you can see the desktop.

Open terminal
- Ctrl + Alt + T  
or
- Windows key -> Accessories -> LXTerminal

In terminal type the following commands, press enter after each one
- cd /home/pi/Documents/
- git clone https://bitbucket.org/supertec-alpha/audio-loop-auchan.git
- cd audio-loop-auchan
- sudo ./setup.sh
- reboot

Once you have run setup.sh your raspberry will start playing an audio file on all audio outputs from it's next boot.

## Informations

Cron informations can be found in assets/cron.  
Startup command can be found in assets/setup.nfo.