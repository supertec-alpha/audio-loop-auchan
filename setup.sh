#!/bin/bash

# Script to setup a player to automatically start playing on boot

# Delete last line of /etc/rc.local
sudo sed -i '$ d' /etc/rc.local
# Add init.sh in startup queue
sudo cat /home/pi/Documents/audio-loop-auchan/assets/setup.nfo >> /etc/rc.local
# Add specific cron to cron pool
sudo cp /home/pi/Documents/audio-loop-auchan/assets/cron /var/spool/cron/audio-loop
# Give correct permission to cron folder
sudo chmod 600 /var/spool/cron/audio-loop
